## Untuk menjalankan App

```bash
composer install
npm install
cp .env.example .env

laravel-echo-server start
npx mix
php artisan serve
```

## Setup

`Redis Windows`
```
https://github.com/microsoftarchive/redis/releases
```

`Laravel Echo Server && Predis`
```bash
npm install -g laravel-echo-server
composer require predis/predis
laravel-echo-server init
laravel-echo-server start
```

## Configuration

`config/app.php`
```
App\Providers\BroadcastServiceProvider::class,
```

```
php artisan make:event TestEvent
```

`Event`
```
class TestEvent implements ShouldBroadcast

protected $message;

    public function __construct($message)
    {
        $this->message = $message;
    }
    
    public function broadcastOn()
    {
        return new Channel('test-channel');
    }

    public function broadcastAs()
    {
        return 'test-event';
    }

    public function broadcastWith()
    {
        return [
            'message' => $this->message
        ];
    }
```

`.env`
```
BROADCAST_DRIVER=redis
REDIS_CLIENT=predis
```

`cmd`
```
npm install --save socket.io-client@2.3.0
npm install --save laravel-echo
```

`resources/js/bootstrap.js`
```
import Echo from 'laravel-echo'
import socketio from 'socket.io-client'

window.Echo = new Echo({
    client: socketio,
    broadcaster: 'socket.io',
    host: window.location.hostname + ':6001'
});

window.Echo.channel('laravel_database_test-channel')
.listen('.test-event', (e) => {
	console.log(e);
});
```

`webpack.mix.js`
```
let mix = require('laravel-mix');
mix.js('resources/js/app.js', 'js');
```

`cli`
```
npm install laravel-mix --save-dev

-------------
npx mix
-------------
npx mix watch
```
